/*
 * grunt-hmh-dependencies
 *
 *
 * Copyright (c) 2016 Dominique Rochefort
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {
  var fs = require('fs');
  var request = require('request');

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask('fetchlib', 'Pull specific HMH dependencies', function() {
    // Merge task-specific and/or target-specific options with these defaults.
    var done = this.async();
    var options = this.options({
      baseUrl: '',
      basePath: '',
      auth: null,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });

    // Iterate over all specified file groups.
    this.data.files.forEach(function(file) {

      var data = '';
      var destination = fs.createWriteStream(options.basePath + file.dest);

      // Poll the server at regular interval until the build is complete
      request({
        method: options.method,
        uri: options.baseUrl + file.src,
        headers: options.headers,
        auth: options.auth
      })
        .pipe(destination)
        .on('finish', done)
        .on('error', done.bind(null, false));
    });
  });

};