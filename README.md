# grunt-fetch-library v0.2.0

> Download a file from any remote location with support for authentication

## Getting Started

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-fetch-library --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-fetch-library');
```


## fetchlib task
_Run this task with the `grunt fetchlib` command._

Task targets, files and options may be specified according to the Grunt [Configuring tasks](http://gruntjs.com/configuring-tasks) guide.

### Options

#### baseUrl
Type: `String`  
Default: `''`

Remote location URL to be prepended to the source file to download. If this string is specified, it must end with a slash.

#### basePath
Type: `String`  
Default: `''`

Local path to be prepended to the destination file to download. If this string is specified, it must end with a slash.

#### auth
Type: `Object`  
Default: `{}`

User authentication for private download sites. Authentication object is based on `request` module parameters.

```js
{
  username: 'user',
  password: 'pass'
}
``` 

#### method
Type: `String`  
Default: `'GET'`

The HTTP method to use.

#### headers
Type: `Boolean` `Object`  
Default: 

```js 
{
  'Content-Type': 'application/json'
}
```

HTTP Headers to send along with the request

### Usage Examples

#### Download a file from a private url

In this example, running `grunt concat:dist` (or `grunt concat` because `concat` is a [multi task][multitask]) will concatenate the three specified source files (in order), joining files with `;` and writing the output to `dist/built.js`.

```js
// Project configuration.
  grunt.initConfig({
    version: '1.0.0',
    fetchlib: {
      options: {
        baseUrl: 'https://bitbucket.org/my/private/repo/downloads/',
        basePath: 'target/dir/',
        auth: {
          user: 'user',
          password: 'pass'
        }
      },
      libs: {
        files: [{
          dest:'lib.js',
          src: 'lib.<%=version%>.min.js'
        }]
      }
    }
  });
```