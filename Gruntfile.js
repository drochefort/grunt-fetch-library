/*
 * grunt-hmh-dependencies
 *
 *
 * Copyright (c) 2016 Dominique Rochefort
 * Licensed under the MIT license.
 */

'use strict';
var pack = require('./package');


module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    versions: pack.hmhDependencies,

    jshint: {
      all: [
        'Gruntfile.js',
        'tasks/*.js'
      ],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    // Configuration to be run (and then tested).
    fetchlib: {
      options: {
        baseUrl: 'https://bitbucket.org/tribalnova/hmh-hab-mtl-widgets-common/downloads/',
        basePath: 'assets/widgets/shared/js/'
      },
      widgetsCommon: {
        files: [{
          dest:'hab-mtl-widgets-common.js',
          src: 'hab-mtl-widgets-common.<%=versions.widgetsCommon%>.min.js'
        }]
      }
    }

  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-nodeunit');

  // Whenever the "test" task is run, first clean the "tmp" dir, then run this
  // plugin's task(s), then test the result.
  grunt.registerTask('test', ['fetchlib']);

  // By default, lint and run all tests.
  grunt.registerTask('default', ['jshint', 'fetchlib']);

};